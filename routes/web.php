<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/dashboards', 'AdminDashboardController@index')->name('admin.dashboard');
});

Route::middleware(['auth', 'author'])->group(function () {
    Route::get('/dashboard', 'AuthorDashboardController@index')->name('author.dashboard');
});
